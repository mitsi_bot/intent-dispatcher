let conf = {
  restricted: true
};

export function getConf(): { restricted: boolean } { return conf; }
export function updateConf(state: boolean): void {
  conf.restricted = state;
}