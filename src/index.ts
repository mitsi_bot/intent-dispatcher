import express, { Response, Request } from "express";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import BaseService from "mitsi_bot_lib/dist/src/BaseService";
import IntentDispatcherInput from "mitsi_bot_lib/dist/src/IntentDispatcherInput";
import dispatch from "./IntentDispatcher"

class Server extends BaseService {
  onPost(request: Request, response: Response): void {
    const startDate = new Date();
    dispatch(request.body as IntentDispatcherInput)
      .then((e: ResponseText) => {
        response.json(e);
        console.debug("TTR:", new Date().getTime() - startDate.getTime());
      }).catch((e: ResponseText) => {
        response.json(e);
        console.debug("TTR:", new Date().getTime() - startDate.getTime());
      });
  }
}

new Server(express()).startServer();

