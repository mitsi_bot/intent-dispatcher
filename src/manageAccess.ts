
import { updateConf, getConf } from "./config"
import { pathOr } from "ramda";
import Entities from "./constants/Entities";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import IntentDispatcherInput from "mitsi_bot_lib/dist/src/IntentDispatcherInput";

function getState(stringState: string) {
  if (stringState == Entities.AUTHORIZATION_ACTIVATED) return false;
  return true;
}

export default function manageAccess(result: IntentDispatcherInput, resolve: Function) {
  const stringState = pathOr(Entities.AUTHORIZATION_DEACTIVATED,
    ["parameters", "fields", "authorization", "stringValue"],
    result
  ) as string;
  const state = getState(stringState);
  updateConf(state);
  const response = getConf().restricted
    ? "Actions restreintes"
    : "Actions ouvertes attention...";
  resolve(new ResponseText(response));
}
