import axios, { AxiosResponse } from 'axios';

import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import ServiceInput from "mitsi_bot_lib/dist/src/ServiceInput";
import IntentDispatcherInput from "mitsi_bot_lib/dist/src/IntentDispatcherInput";
import manageAccess from './manageAccess';
import Intents from './constants/Intents';
const config = require('config-yml');

export default function dispatch(intentDispatcherInput: IntentDispatcherInput): Promise<ResponseText> {
  return new Promise(async (resolve, reject) => {
    if (!intentDispatcherInput || !intentDispatcherInput.intent) {
      reject(new ResponseText("Result intent is undefined..."));
      return;
    }

    //To refactor
    if (intentDispatcherInput.intent === Intents.AUTHORIZATION) {
      console.debug("Detected Intent", intentDispatcherInput.intent);
      manageAccess(intentDispatcherInput, resolve);
      return;
    }

    const service = await findService(config.webservices, intentDispatcherInput.intent);
    console.debug("Detected Intent, in", intentDispatcherInput.intent, service);

    if (!service) {
      reject(new ResponseText("No associated action"));
      return;
    }
    if (service.secured && !intentDispatcherInput.isAuthenticated) {
      reject(new ResponseText("Access not granted."));
      return;
    }

    const data = new ServiceInput(intentDispatcherInput.parameters);
    axios
      .post(service.uri, data)
      .then((response: AxiosResponse<ResponseText>) => {
        resolve(response.data);
      })
      .catch((err: Error) => {
        console.error("Process message", err);
        resolve(new ResponseText("An error has occured"));
      });
  });
}

function findService(services: any[], intentName?: string | null): any | undefined {
  return new Promise((res, _) => {
    services.forEach(service => {
      Object.keys(service).forEach((key: any) => {
        if (service[key].intent === intentName) {
          res(service[key]);
          return;
        }
      });
    });
    res(undefined);
  });
}
