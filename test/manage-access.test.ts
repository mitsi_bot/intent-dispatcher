import ResponseText from 'mitsi_bot_lib/dist/src/ResponseText';
import IntentDispatcherInput from 'mitsi_bot_lib/dist/src/IntentDispatcherInput';
import manageAccess from '../src/manageAccess';

const activatedQueryResult = new IntentDispatcherInput("Authorization", false, {
    fields: {
        authorization: {
            stringValue: "ACTIVATED"
        }
    }
});

const deactivatedQueryResult = new IntentDispatcherInput("Authorization", false, {
    fields: {
        authorization: {
            stringValue: "DEACTIVATED"
        }
    }
});

describe("Manage access", () => {
    it("should active all functionnalities for everyone", done => {
        manageAccess(activatedQueryResult,
            jest.fn((r: ResponseText) => {
                expect(r.displayText).toBe("Actions ouvertes attention...");
                done();
            })
        );
    });

    it("should deactive functionnalities for everyone", done => {
        manageAccess(deactivatedQueryResult,
            jest.fn((r: ResponseText) => {
                expect(r.displayText).toBe("Actions restreintes");
                done();
            })
        );
    });

    it("should restrict functionnalities by default", done => {
        manageAccess(deactivatedQueryResult,
            jest.fn((r: ResponseText) => {
                expect(r.displayText).toBe("Actions restreintes");
                done();
            })
        );
    });
});